package com.avenuecode.crud.domain;

import com.avenuecode.crud.dto.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor

public class Product implements Indexable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description;

    public Product(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Product(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Transient
    private List<Image> imageList;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Product.class)
    @JoinColumn(name = "parent_product_id")
    private Product parent;

    @Transient
    private List<Product> childList;

    public ProductDTO toDTO() {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(id);
        productDTO.setName(name);
        productDTO.setDescription(description);
        if (getParent() != null) {
            productDTO.setParentId(getParent().getId());
        }
        if (getImageList() != null) {
            productDTO.setImageList(getImageList().stream().map(Image::toDTO).collect(Collectors.toList()));
        }
        if (getChildList() != null) {
            productDTO.setChildList(getChildList().stream().map(Product::toDTO).collect(Collectors.toList()));
        }
        return productDTO;
    }
}
