# CRUD FOR CODEAVENUE [ ![Codeship Status for barbatchov/com.avenuecode.crud](https://app.codeship.com/projects/e7f89590-bd97-0135-7dd4-3ec1750d8459/status?branch=master)](https://app.codeship.com/projects/259740) #

This project is for test purposes.

### How to build ###

Running the build:
```
./wmvn package
```

### How to run the application ###

Running the application:
```
./mvnw spring-boot:run
```

### How to run the tests ###

Running the application:
```
./mvnw test
```
