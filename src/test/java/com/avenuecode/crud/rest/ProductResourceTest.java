package com.avenuecode.crud.rest;

import com.avenuecode.crud.domain.Image;
import com.avenuecode.crud.domain.Product;
import com.avenuecode.crud.dto.ProductDTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@DatabaseSetup("/dbunit/ProductResourceTest.xml")
public class ProductResourceTest {
    private static final String API = "http://localhost:8080/image";

    @LocalServerPort
    private int port;

    private URI uri;

    @Before
    public void setUp() throws URISyntaxException {
        this.uri = new URI("http://localhost:" + this.port);
    }

    @Test
    public void shouldInsert() {
        ProductDTO product = new ProductDTO();
        product.setDescription("blah");
        product.setName("product w");
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").request()
            .put(Entity.entity(product, MediaType.APPLICATION_JSON));
        Assert.assertTrue(response.getStatus() == 200);
        ProductDTO result = response.readEntity(ProductDTO.class);
        Assert.assertTrue(result.getId() == 5);
        Assert.assertTrue(result.getName().equals("product w"));
        Assert.assertTrue(result.getDescription().equals("blah"));
    }

    @Test
    public void shouldGet() throws Exception {
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").path("1").request()
            .get();
        Assert.assertTrue(response.getStatus() == 200);
        ProductDTO result = response.readEntity(ProductDTO.class);
        Assert.assertTrue(result.getId() == 1);
    }

    @Test
    public void shouldUpdate() throws Exception {
        ProductDTO product = new ProductDTO();
        product.setId(3);
        product.setName("product updated");
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").request()
            .post(Entity.entity(product, MediaType.APPLICATION_JSON));
        Assert.assertTrue(response.getStatus() == 200);
        ProductDTO result = response.readEntity(ProductDTO.class);
        Assert.assertTrue(result.getId() == 3);
        Assert.assertTrue(result.getName().equals("product updated"));
    }

    @Test
    public void shouldDelete() throws Exception {
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").path("1").request()
            .delete();
        Assert.assertTrue(response.getStatus() == 204);
    }

    @Test
    public void shouldGetAllProductsExcludingRelateds() {
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").path("all-excluding-related").request()
            .get();
        List<Map> result = response.readEntity(List.class);
        Assert.assertTrue(response.getStatus() == 200);
        Assert.assertTrue((Integer) result.get(0).get("id") == 1);
        Assert.assertNull(result.get(0).get("parent"));
        Assert.assertTrue((Integer) result.get(1).get("id") == 2);
        Assert.assertNull(result.get(1).get("parent"));
        Assert.assertTrue((Integer) result.get(2).get("id") == 3);
        Assert.assertNull(result.get(2).get("parent"));
        Assert.assertTrue((Integer) result.get(3).get("id") == 4);
        Assert.assertNull(result.get(3).get("parent"));
    }

    @Test
    public void shouldGetAllProductsIncludingRelateds() {
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").path("all-including-related").request()
            .get();
        List<Map> result = response.readEntity(List.class);
        Assert.assertTrue(response.getStatus() == 200);
        Assert.assertTrue((Integer) result.get(0).get("id") == 1);
        Assert.assertTrue(((List) result.get(0).get("imageList")).size() == 3);
        Assert.assertTrue((Integer) result.get(1).get("id") == 2);
        Assert.assertTrue(((List) result.get(1).get("imageList")).size() == 3);
        Assert.assertTrue((Integer) result.get(2).get("id") == 3);
        Assert.assertTrue(((List) result.get(2).get("imageList")).size() == 0);
        Assert.assertTrue((Integer) result.get(3).get("id") == 4);
        Assert.assertTrue(((List) result.get(3).get("imageList")).size() == 0);
    }

    @Test
    public void shouldSetImagesForAProduct() {
        List<Image> imageList = Arrays.asList(
            new Image(3),
            new Image(4)
        );
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").path("3").path("set-images").request()
            .put(Entity.entity(imageList, MediaType.APPLICATION_JSON));
        ProductDTO result = response.readEntity(ProductDTO.class);
        Assert.assertTrue(result.getImageList().size() == 2);
    }

    @Test
    public void shouldSetChildrenForAProduct() {
        List<Product> childrenList = Arrays.asList(
            new Product("pepa pig", "doll"),
            new Product("bozo", "blu-ray")
        );
        Response response = ClientBuilder.newClient()
            .target(uri).path("product").path("3").path("set-children").request()
            .put(Entity.entity(childrenList, MediaType.APPLICATION_JSON));
        ProductDTO result = response.readEntity(ProductDTO.class);
        Assert.assertTrue(result.getChildList().size() == 2);
    }
}
