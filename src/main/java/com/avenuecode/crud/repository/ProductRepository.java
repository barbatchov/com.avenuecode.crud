package com.avenuecode.crud.repository;

import com.avenuecode.crud.domain.Product;
import com.avenuecode.crud.dto.ProductDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    @Query("select new Product(p.id, p.name, p.description) from Product p")
    List<Product> getAllExcludingRelated();

    @Query("SELECT p FROM Product p WHERE p.parent.id = :id")
    List<Product> getAllChildren(@Param("id") Integer id);
}
