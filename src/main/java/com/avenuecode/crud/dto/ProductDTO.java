package com.avenuecode.crud.dto;

import com.avenuecode.crud.domain.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class ProductDTO {
    private Integer id;
    private String name;
    private String description;
    private List<ImageDTO> imageList;
    private Integer parentId;
    private List<ProductDTO> childList;

    public ProductDTO(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Product toProduct() {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setDescription(description);
        if (parentId != null) {
            Product parent = new Product();
            parent.setId(parentId);
            product.setParent(parent);
        }
        if (imageList != null) {
            product.setImageList(imageList.stream().map(ImageDTO::toImage).collect(Collectors.toList()));
        }
        if (childList != null) {
            product.setChildList(childList.stream().map(ProductDTO::toProduct).collect(Collectors.toList()));
        }
        return product;
    }
}
