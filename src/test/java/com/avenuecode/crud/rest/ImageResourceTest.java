package com.avenuecode.crud.rest;

import com.avenuecode.crud.dto.ImageDTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@DatabaseSetup("/dbunit/ImageResourceTest.xml")
public class ImageResourceTest {

    private static final String API = "http://localhost:8080/image";

    @LocalServerPort
    private int port;

    private URI uri;

    @Before
    public void setUp() throws URISyntaxException {
        this.uri = new URI("http://localhost:" + this.port);
    }

    @Test
    public void shouldInsert() {
        ImageDTO image = new ImageDTO();
        image.setType(1);
        Response response = ClientBuilder.newClient()
            .target(uri).path("image").request()
            .put(Entity.entity(image, MediaType.APPLICATION_JSON));
        Assert.assertTrue(response.getStatus() == 200);
        ImageDTO result = response.readEntity(ImageDTO.class);
        Assert.assertTrue(result.getId() == 4);
        Assert.assertTrue(result.getType() == 1);
    }

    @Test
    public void shouldGet() throws Exception {
        Response response = ClientBuilder.newClient()
            .target(uri).path("image").path("1").request()
            .get();
        Assert.assertTrue(response.getStatus() == 200);
        ImageDTO result = response.readEntity(ImageDTO.class);
        Assert.assertTrue(result.getId() == 1);
        Assert.assertTrue(result.getType() == 1);
    }

    @Test
    public void shouldUpdate() throws Exception {
        ImageDTO image = new ImageDTO();
        image.setId(3);
        image.setType(2);
        Response response = ClientBuilder.newClient()
            .target(uri).path("image").request()
            .post(Entity.entity(image, MediaType.APPLICATION_JSON));
        Assert.assertTrue(response.getStatus() == 200);
        ImageDTO result = response.readEntity(ImageDTO.class);
        Assert.assertTrue(result.getId() == 3);
        Assert.assertTrue(result.getType() == 2);
    }

    @Test
    public void shouldDelete() throws Exception {
        Response response = ClientBuilder.newClient()
            .target(uri).path("image").path("1").request()
            .delete();
        Assert.assertTrue(response.getStatus() == 204);
    }

    @Test
    public void shouldGetAllProductsExcludingRelateds() {
        Response response = ClientBuilder.newClient()
            .target(uri).path("image").path("all-excluding-related").request()
            .get();
        List<Map> result = response.readEntity(List.class);
        Assert.assertTrue(response.getStatus() == 200);
        Assert.assertTrue((Integer) result.get(0).get("id") == 1);
        Assert.assertNull(result.get(0).get("product"));
        Assert.assertTrue((Integer) result.get(1).get("id") == 2);
        Assert.assertNull(result.get(1).get("product"));
        Assert.assertTrue((Integer) result.get(2).get("id") == 3);
        Assert.assertNull(result.get(2).get("product"));
    }

    @Test
    public void shouldGetAllProductsIncludingRelateds() {
        Response response = ClientBuilder.newClient()
            .target(uri).path("image").path("all-including-related").request()
            .get();
        List<Map> result = response.readEntity(List.class);
        Assert.assertTrue(response.getStatus() == 200);
        Assert.assertTrue((Integer) result.get(0).get("id") == 1);
        Assert.assertNotNull(result.get(0).get("product"));
        Assert.assertTrue((Integer) result.get(1).get("id") == 2);
        Assert.assertNotNull(result.get(1).get("product"));
        Assert.assertTrue((Integer) result.get(2).get("id") == 3);
        Assert.assertNotNull(result.get(2).get("product"));
    }
}
