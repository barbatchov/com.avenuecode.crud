package com.avenuecode.crud.dto;

import com.avenuecode.crud.domain.Image;
import com.avenuecode.crud.domain.Product;
import lombok.Data;

@Data
public class ImageDTO {
    private Integer id;
    private Integer type;
    private Integer productId;

    public Image toImage() {
        Image image = new Image();
        image.setId(id);
        image.setType(type);
        if (productId != null) {
            Product product = new Product();
            product.setId(productId);
            image.setProduct(product);
        }
        return image;
    }
}
