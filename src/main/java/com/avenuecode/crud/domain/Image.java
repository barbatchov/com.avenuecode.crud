package com.avenuecode.crud.domain;

import com.avenuecode.crud.dto.ImageDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.ws.rs.PUT;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Image implements Indexable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer type;

    @ManyToOne//(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Product.class)
    @JoinColumn(name = "product_id", foreignKey = @ForeignKey(name = "fk_product_product_id"))
    private Product product;

    public Image(Integer id, Integer type) {
        this.id = id;
        this.type = type;
    }

    public Image(Integer type) {
        this.type = type;
    }

    public ImageDTO toDTO() {
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setId(id);
        imageDTO.setType(type);
        if (getProduct() != null) {
            imageDTO.setProductId(getProduct().getId());
        }
        return imageDTO;
    }
}
