package com.avenuecode.crud.repository;

import com.avenuecode.crud.domain.Image;
import com.avenuecode.crud.domain.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Stream;

@Repository
@Transactional
public interface ImageRepository extends CrudRepository<Image, Integer> {
    Stream<Image> findByProductId(Integer id);

    @Query("SELECT new Image(p.id, p.type) FROM Image p WHERE p.product.id = :id")
    List<Image> getAllExcludingRelatedByProductId(@Param("id") Integer id);

    @Query("SELECT new Image(p.id, p.type) FROM Image p")
    List<Image> getAllExcludingRelated();
}
