package com.avenuecode.crud.config;

import com.avenuecode.crud.rest.ImageResource;
import com.avenuecode.crud.rest.ProductResource;
import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@Configuration
public class JerseyConfig extends ResourceConfig {

    private final BeanConfig beanConfig;

    public JerseyConfig() {
        this.beanConfig = new BeanConfig();
        registerEndpoints();
        configureSwagger();
    }

    private void configureSwagger() {
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/");
        beanConfig.setResourcePackage("com.avenuecode.crud.rest");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }

    private void registerEndpoints() {
        register(ProductResource.class);
        register(ImageResource.class);
    }
}
