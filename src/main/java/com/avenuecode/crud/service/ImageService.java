package com.avenuecode.crud.service;

import com.avenuecode.crud.domain.Image;
import com.avenuecode.crud.domain.Product;
import com.avenuecode.crud.repository.ImageRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Data
@Service
@Transactional
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    public Iterable<Image> getAllIncludingRelated() {
        Iterable<Image> all = imageRepository.findAll();
        return all;
    }
}
