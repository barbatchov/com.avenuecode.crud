package com.avenuecode.crud.domain;

public interface Indexable {
    void setId(Integer id);
    Integer getId();
}
