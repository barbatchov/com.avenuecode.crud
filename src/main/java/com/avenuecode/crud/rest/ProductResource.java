package com.avenuecode.crud.rest;

import com.avenuecode.crud.domain.Product;
import com.avenuecode.crud.dto.ImageDTO;
import com.avenuecode.crud.dto.ProductDTO;
import com.avenuecode.crud.service.ProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

@Api
@Component
@Path("/product")
public class ProductResource {
    @Autowired
    ProductService service;

    @GET
    @Path("/{id}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public ProductDTO get(@PathParam("id") Integer id) {
        return service.getProductRepository().findOne(id).toDTO();
    }

    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public ProductDTO put(ProductDTO product) {
        return service.getProductRepository().save(product.toProduct()).toDTO();
    }

    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public ProductDTO post(Product product) {
        if (product.getId() == null) {
            throw new InvalidParameterException("Id must not be null");
        }
        return service.getProductRepository().save(product).toDTO();
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Integer id) {
        service.delete(id);
    }

    @GET
    @Path("/all-excluding-related")
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Product> getAllExcludingRelated() {
        return service.getProductRepository().getAllExcludingRelated();
    }

    @GET
    @Path("/all-including-related")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Iterable<Product> getAllIncludingRelated() {
        return service.getAllIncludingRelated();
    }

    @PUT
    @Path("/{id}/set-images")
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public ProductDTO setImages(@PathParam("id") Integer id, List<ImageDTO> imageList) {
        return service.setImages(id, imageList).toDTO();
    }

    @PUT
    @Path("/{id}/set-children")
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public ProductDTO setChildren(@PathParam("id") Integer id, List<ProductDTO> productList) {
        List<Product> list = productList.stream().map(productDTO -> {
            productDTO.setParentId(id);
            return productDTO.toProduct();
        }).collect(Collectors.toList());
        return service.setChildren(id, list).toDTO();
    }
}
