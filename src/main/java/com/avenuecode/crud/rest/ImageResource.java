package com.avenuecode.crud.rest;

import com.avenuecode.crud.domain.Image;
import com.avenuecode.crud.domain.Product;
import com.avenuecode.crud.dto.ImageDTO;
import com.avenuecode.crud.service.ImageService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api
@Component
@Path("/image")
public class ImageResource {
    @Autowired
    ImageService service;

    @GET
    @Path("/{id}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public ImageDTO get(@PathParam("id") Integer id) {
        return service.getImageRepository().findOne(id).toDTO();
    }

    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public ImageDTO put(ImageDTO imageDTO) {
        return service.getImageRepository().save(imageDTO.toImage()).toDTO();
    }

    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public ImageDTO post(ImageDTO imageDTO) {
        return service.getImageRepository().save(imageDTO.toImage()).toDTO();
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Integer id) {
        service.getImageRepository().delete(id);
    }

    @GET
    @Path("/all-excluding-related")
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Image> getAllExcludingRelated() {
        return service.getImageRepository().getAllExcludingRelated();
    }

    @GET
    @Path("/all-including-related")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Iterable<Image> getAllIncludingRelated() {
        return service.getAllIncludingRelated();
    }

}
