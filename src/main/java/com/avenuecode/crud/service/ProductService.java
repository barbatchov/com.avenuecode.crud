package com.avenuecode.crud.service;

import com.avenuecode.crud.domain.Image;
import com.avenuecode.crud.domain.Product;
import com.avenuecode.crud.dto.ImageDTO;
import com.avenuecode.crud.repository.ImageRepository;
import com.avenuecode.crud.repository.ProductRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Transactional
    public void delete(Integer id) {
        Product product = productRepository.findOne(id);
        if (product != null) {
            imageRepository.delete(imageRepository.findByProductId(id).collect(Collectors.toList()));
        }
        productRepository.delete(id);
    }

    @Transactional
    public Iterable<Product> getAllIncludingRelated() {
        Iterable<Product> all = productRepository.findAll();
        all.forEach(product -> {
            product.setImageList(imageRepository.getAllExcludingRelatedByProductId(product.getId()));
            product.setChildList(productRepository.getAllChildren(product.getId()));
        });
        return all;
    }

    @Transactional
    public Product setImages(Integer id, List<ImageDTO> imageList) {
        List<Image> list = imageList.stream().map(imageDTO -> {
            imageDTO.setProductId(id);
            return imageDTO.toImage();
        }).collect(Collectors.toList());
        imageRepository.save(list);
        Product product = productRepository.findOne(id);
        product.setImageList(list);
        return product;
    }

    @Transactional
    public Product setChildren(Integer id, List<Product> productList) {
        productRepository.save(productList);
        Product product = productRepository.findOne(id);
        product.setChildList(productList);
        return product;
    }
}
